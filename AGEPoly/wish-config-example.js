module.exports = {
    mongodb_url: "mongodb://localhost:27017/",
    /* Emails sender identity */
    mail: "Wish test<wish@localhost>",
    /* SMTP Connection configuration */
    host: "localhost",
    port: 465,
    user: "",
    password: "",
    /* Admin password to access the history of all events */
    history_password: "",
};
