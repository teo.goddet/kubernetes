{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Werror=missing-fields #-}

-- (C) AGEPoly (Association Générale des Etudiants de l’EPFL)
--     Roosembert Palacios, 2019
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- This file is part of Silhouette

module Silhouette.Processing where

import Control.Exception (try, SomeException)
import Control.Monad.Except
import Data.Aeson
import Data.Aeson.TH (deriveToJSON, defaultOptions)
import Data.Char (isAlphaNum, isAscii)
import Data.Either.Combinators
import Data.Maybe
import Data.Monoid
import Data.Char
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import Silhouette.Types (AppConfiguration(..), AppSecrets(..))
import System.FilePath.Posix (joinPath, takeFileName)

import qualified Data.ByteString as BS
import qualified Data.ByteString.Base64 as BS (encode)
import qualified Data.HashMap.Lazy as HM
import qualified Data.Text as T
import qualified Silhouette.Readers.ApplicationSpec as AS
import qualified Silhouette.Readers.SecretSpec as SS
import qualified Silhouette.TemplateManagement as TM

type App = Text  -- An Application is identified by its name

data DomainName = DN Text

getFqdn :: DomainName -> Text
getFqdn (DN fqdn) = fqdn

instance Semigroup DomainName where
  DN a <> DN b | b == "." = DN a
  DN a <> DN b | a == "." = DN b
  DN a <> DN b | otherwise = DN $ a <> "." <> b

instance Monoid DomainName where
  mempty = DN "."

mkMainDomain :: Text -> Text -> Text -> Bool -> DomainName
mkMainDomain subdomain mountDomain environment useTld =
    mconcat [ subdomain `unless` (subdomain == ".")
            , environment `unless` useTld
            , DN mountDomain
            ]
  where unless part cond = if cond then mempty else (DN part)

mkAliasDomain :: Text -> Bool -> Text -> DomainName
mkAliasDomain environment useTld alias =
    mconcat [ environment `unless` useTld, DN alias ]
  where unless part cond = if cond then mempty else (DN part)

tlsSecretForDomain :: DomainName -> TM.TlsSource
tlsSecretForDomain (DN fqdn) = TM.TlsSecret (name fqdn)
  where name fqdn | (T.length . T.filter (== '.') $ fqdn) == 1 = fqdn <> "-ssl"
        name fqdn | otherwise = (T.tail $ T.dropWhile (/= '.') fqdn) <> "-wildcard-ssl"

envVarsFromSecret :: App -> AS.AppSecrets -> [TM.EnvVar]
envVarsFromSecret appName AS.AppSecrets{environment} = fmap mkSecretEnvVar environment
  where mkSecretEnvVar secret = TM.SecretEnvVar envVarName secretName secretKey
          where envVarName = extractName secret
                secretName = appName <> "-env-secrets"
                secretKey = extractName secret
        extractName AS.SecretEnvVar{name} = name

envVarsForDomains :: [DomainName] -> [TM.EnvVar]
envVarsForDomains domains = [TM.NormalEnvVar "HOST" $ T.intercalate ":" (getFqdn <$> domains)]

b64EncodeText = decodeUtf8 . BS.encode . encodeUtf8

envSecretsName = (<> "-env-secrets")
fileSecretsName = (<> "-file-secrets")

sanitizeConfigKey :: String -> String
sanitizeConfigKey = map replaceIllegalChars
  where replaceIllegalChars c | isAscii c && isAlphaNum c = Data.Char.toLower c
        replaceIllegalChars _ = '-'


secretGroupFromFile :: Text -> App -> SS.SecretsFile -> Either Text [SS.DetachedSecret]
secretGroupFromFile group appName (SS.SecretsFile ss) = maybeToRight errorMsg . HM.lookup group =<< secretsInCollection
  where errorMsg = "Could not find secret group " <> group <> " for application " <> appName
        secretsInCollection = maybeToRight errorMsg (SS.secrets <$> mAppSecret)
          where mAppSecret = listToMaybe $ filter ((appName ==) . SS.appName) ss
                errorMsg = "Could not find app secrets for " <> appName

selectEnvSecrets :: SS.SecretsFile -> App -> [AS.SecretEnvVar] -> Either Text [SS.DetachedSecret]
selectEnvSecrets secretsFile appName = mapM (lookupSecret . extractKey)
  where lookupSecret key = (maybeToRight errorMsg . listToMaybe . filter filterFn) =<< availableSecrets
          where availableSecrets = secretGroupFromFile (envSecretsName appName) appName secretsFile
                filterFn SS.SecretEnvVar{..} = name == key
                filterFn _ = False -- Secret of the wrong type!
                errorMsg = "Could not find secret for environment variable " <> key
        extractKey AS.SecretEnvVar{..} = name

selectFileSecrets :: SS.SecretsFile -> App -> [AS.SecretFile] -> Either Text [SS.DetachedSecret]
selectFileSecrets secretsFile appName = mapM (lookupSecret . extractKey)
  where lookupSecret key = (maybeToRight errorMsg . listToMaybe . filter filterFn) =<< availableSecrets
          where availableSecrets = secretGroupFromFile (fileSecretsName appName) appName secretsFile
                filterFn SS.SecretFile{path=pathInSecret} = T.pack pathInSecret == key
                filterFn _ = False -- Secret of the wrong type!
                errorMsg = "Could not find secret for file " <> key
        extractKey AS.SecretFileInline{..} = T.pack path
        extractKey AS.SecretFileInFile{..} = T.pack path

secretsFromFile :: FilePath -> SS.SecretsFile -> AS.Application -> ExceptT Text IO AppSecrets
secretsFromFile pathPrefix secretsFile AS.Application{appName, secrets = requiredSecrets} = do
  let interpretSecret :: SS.DetachedSecret -> ExceptT Text IO TM.SecretAtom
      interpretSecret SS.SecretEnvVar{name, value = plainValue} = lift . pure $ TM.SecretAtom name (b64EncodeText plainValue)
      interpretSecret SS.SecretFile{..} = ExceptT (mapLeft onErr <$> try (TM.SecretAtom sanitizedName <$> value))
        where value = decodeUtf8 . BS.encode <$> BS.readFile (joinPath [pathPrefix, file])
              sanitizedName = T.pack $ sanitizeConfigKey path
              onErr :: SomeException -> Text
              onErr exc = "Could not load secret file for app " <> appName <> ": " <> T.pack (show exc)

      requiredEnvSecrets = maybe [] AS.environment requiredSecrets
      requiredFileSecrets = maybe [] AS.files requiredSecrets

      realizeSecrets fn ss = mapM interpretSecret =<< ExceptT (return $ fn secretsFile appName ss)

  envSecrets <- TM.Secret (envSecretsName appName) <$> realizeSecrets selectEnvSecrets requiredEnvSecrets
  fileSecrets <- TM.Secret (fileSecretsName appName) <$> realizeSecrets selectFileSecrets requiredFileSecrets

  pure AppSecrets{appName, secrets = [envSecrets, fileSecrets]}

secretsFromExamples :: FilePath -> AS.Application -> IO AppSecrets
secretsFromExamples basepath AS.Application{..} = do
  let realizeEnvSecret AS.SecretEnvVar{..} = pure $ TM.SecretAtom name (b64EncodeText example)
      realizeFileSecret AS.SecretFileInline{..} = pure $ TM.SecretAtom sanitizedName (b64EncodeText example)
        where sanitizedName = T.pack $ sanitizeConfigKey path
      realizeFileSecret AS.SecretFileInFile{..} = do
        example <- readFile (joinPath [basepath, T.unpack exampleFile])
        return $ TM.SecretAtom sanitizedName (b64EncodeText $ T.pack example)
        where sanitizedName = T.pack $ sanitizeConfigKey path
      mkEnvSecret secrets = TM.Secret (appName <> "-env-secrets") <$> mapM realizeEnvSecret secrets
      mkFileSecret secrets = TM.Secret (appName <> "-file-secrets") <$> mapM realizeFileSecret secrets

  envSecrets <- mkEnvSecret $ maybe [] AS.environment secrets
  fileSecrets <- mkFileSecret $ maybe [] AS.files secrets
  pure $ AppSecrets{appName, secrets = [envSecrets, fileSecrets]}

volumesFromSecret :: App -> AS.AppSecrets -> [TM.Volume]
volumesFromSecret appName AS.AppSecrets{files} = fmap mkVolume files
  where mkVolume AS.SecretFileInline{..} = mkAppSecretVol path fileMode
        mkVolume AS.SecretFileInFile{..} = mkAppSecretVol path fileMode
        mkAppSecretVol path fileMode = TM.SecretsVolume secretName (secretPath path fileMode)
          where secretName = appName <> "-secrets-" <> T.pack (sanitizeConfigKey path)
        secretPath path fileMode = TM.SVS (appName <> "-file-secrets") (fromMaybe 292 fileMode) [mkItem path]
        mkItem path = TM.KeyToPath (sanitizeConfigKey path) (takeFileName path)

volumeMountsFromSecret :: App -> AS.AppSecrets -> [TM.VolumeMount]
volumeMountsFromSecret appName AS.AppSecrets{..} = fmap mkVolumeMount files
  where mkVolumeMount AS.SecretFileInline{..} = mkAppSecretVolMnt path
        mkVolumeMount AS.SecretFileInFile{..} = mkAppSecretVolMnt path
        mkAppSecretVolMnt path =
          TM.VolumeMount { name = mountName, mountPath = path, subPath = Just (takeFileName path) }
          where mountName = appName <> "-secrets-" <> T.pack (sanitizeConfigKey path)

data AppProcessorOptions = ProcessorOptions
  { mountDomain :: Text
  , environment :: Text
  , useTld :: Bool
  , lockImages :: Maybe Text
  , tagImages :: Maybe Text
  }

-- | The 'lockImageName' function computes the image name used by the deployment
-- | honoring the specified image locking options
lockImageName ::
     App -- ^ Application name
  -> Maybe Text -- ^ If specified, force a new image registry, if empty the local registry will be used
  -> Maybe Text -- ^ If specified, force a new image tag
  -> Text -- ^ Original name of the image
  -> Text -- ^ New image name
lockImageName appName mreg mtag image = lockReg mreg $ lockTag mtag image
  where lockTag Nothing name = name
        lockTag (Just tag) name = fst (T.breakOn ":" name) <> ":" <> tag
        lockReg Nothing name = name
        lockReg (Just reg) name | reg == "" = appName <> snd (T.breakOn ":" name)
                                | otherwise = reg <> "/" <> appName <> snd (T.breakOn ":" name)

mkAppConfiguration :: AppProcessorOptions -> AS.Application -> AppConfiguration
mkAppConfiguration ProcessorOptions{..} AS.Application{..} = AppConfiguration
  TM.Deployment
    { appName
    , appOwner = owner
    , imagePullSecrets = [ "gitlab-kubernetes" ] ++ (maybeToList imagePullSecretName)
    , imageName = lockImageName appName lockImages tagImages imageName
    , initCommand = fromMaybe "" initCommand
    , initImageName = fromMaybe "busybox" initImageName
    , port
    , envvars = maybe [] (envVarsFromSecret appName) secrets
             ++ fmap mkEnvVar envVars
             ++ envVarsForDomains (mainDomain : aliasDomains)
    , mainDomain = getFqdn mainDomain
    , volumeMounts = fmap mkMount volumes
                  ++ maybe [] (volumeMountsFromSecret appName) secrets
    , volumes = fmap mkVolume volumes
             ++ maybe [] (volumesFromSecret appName) secrets
    , probePath = fromMaybe "/" probePath
    }
  TM.Service
    { appName
    , appOwner = owner
    , port
    }
  [ TM.IngressRoute
    { ingressRouteName = appName <> "-main-domain"
    , appName
    , appOwner = owner
    , routes = [getFqdn mainDomain]
    , tlsSource = tlsSecretForDomain mainDomain
    },
    TM.IngressRoute
    { ingressRouteName = appName <> "-aliases"
    , appName
    , appOwner = owner
    , routes = getFqdn <$> aliasDomains
    , tlsSource = TM.LeTlsResolver
    } ]
  [ TM.PersistentVolume
    { appName
    , appOwner = owner
    , name = appName <> "-" <> volumeName v <> "-" <> environment <> "-pv"
    , volume = v
    , environment = environment
    } | v <- volumes ]
  [ TM.PersistentVolumeClaim
    { appName
    , appOwner = owner
    , name = appName <> "-" <> volumeName v <> "-pvc"
    , volume = v
    , environment = environment
    } | v <- volumes ]
  where mainDomain = mkMainDomain subdomain mountDomain environment useTld
        aliasDomains = mkAliasDomain environment useTld <$> dnsAliases
        volName specName = appName <> "-" <> specName <> "-v"
        mkEnvVar :: AS.EnvVar -> TM.EnvVar
        mkEnvVar AS.EnvVar{..} = TM.NormalEnvVar name value
        mkMount :: AS.Volume -> TM.VolumeMount
        mkMount AS.Volume{..} = TM.VolumeMount (volName name) path Nothing
        mkVolume :: AS.Volume -> TM.Volume
        mkVolume AS.Volume{..} = TM.PvcVolume (volName name) (TM.PVC pvcName)
          where pvcName = appName <> "-" <> name <> "-pvc"
        volumeName AS.Volume{..} = name
